from gensim import corpora
from gensim import models
import numpy as np
import pandas as pd
from plotly.offline import plot
import plotly.graph_objs as go
from sklearn.decomposition import IncrementalPCA
from sklearn.manifold import TSNE

np.random.seed(12345)

def __data_utils__(path):
    raw = pd.read_csv(path, names = ['Date', 'ID', 'Product'])
    raw['Date'] = pd.to_datetime(raw['Date'])
    grouped = raw.groupby(['Date', 'ID'])
    new = pd.DataFrame(columns = ['Date', 'ID', 'Cart'])
    for id, grp in grouped:
        cart = grp['Product'].values
        row = pd.Series(list(id + (cart, )), index = ['Date', 'ID', 'Cart'])
        new = new.append(row, ignore_index = True)
    return new

def tsne_plot(model):
    num_dim = 2
    vec = []
    lab = []

    for w in model.wv.vocab:
        vec.append(model[w])
        lab.append(w)

    vec = np.asarray(vec)
    tsne = TSNE(n_components = num_dim, random_state = 42)
    vec = tsne.fit_transform(vec)

    x_vals = [v[0] for v in vec]
    y_vals = [v[1] for v in vec]

    trace = go.Scatter(
        x = x_vals,
        y = y_vals,
        mode = 'text',
        text = lab
    )

    plot([trace], filename = 'w2v_plot.html')

if __name__ == "__main__":
    raw_data = __data_utils__('dataset_group.csv')
    raw_data.Cart = raw_data.Cart.apply(
        lambda x: np.random.choice(x, len(x), replace = False))
    corpus = raw_data.Cart.values.tolist()
    corpus = [list(arr) for arr in corpus]

    word2vec = models.Word2Vec(min_count = 1, size = 100, workers = 4)
    word2vec.build_vocab(corpus)
    word2vec.train(corpus, total_examples = word2vec.corpus_count, epochs = word2vec.epochs)
    # print(word2vec)
    # print(word2vec.wv.vocab)
    # print(word2vec.most_similar(positive = ['flour']))
    tsne_plot(word2vec)
